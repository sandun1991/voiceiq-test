resource "aws_lb" "public_alb" {
  name                        = "voiceiq-alb"
  subnets                     = ["${aws_subnet.public-voiceiq-us-east-1a.id}", "${aws_subnet.public-voiceiq-us-east-1b.id}"]
  security_groups             = ["${aws_security_group.web.id}"]
  idle_timeout                = "${var.elb.["idle_timeout"]}"

  enable_deletion_protection  = true
  
}

/* Target Group Default*/
resource "aws_lb_target_group" "alb_default_tg" {
  name = "default-voiceiq"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.voiceiq_vpc.id}"
}

/* Create listener */
resource "aws_lb_listener" "public_alb_80" {

  depends_on = ["aws_lb_target_group.alb_default_tg"]

  load_balancer_arn = "${aws_lb.public_alb.arn}"
  port              = "80"
  protocol          = "HTTP"
  
  default_action {
    target_group_arn = "${aws_lb_target_group.alb_default_tg.arn}"
    type             = "forward"
  }
}
