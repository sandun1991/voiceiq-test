resource "aws_nat_gateway" "voiceiq_nat" {
    allocation_id = "${aws_eip.voiceiq_eip.id}"
    subnet_id = "${aws_subnet.public-voiceiq-us-east-1b.id}"
}
