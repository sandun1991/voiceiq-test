resource "aws_internet_gateway" "voiceiq_igw" {
    vpc_id = "${aws_vpc.voiceiq_vpc.id}"
    tags {
        Name = "VoiceIQ IGW"
    }
}
