variable "db_mysql_count" {
  default = 1
}

/* DB Servers */
resource "aws_instance" "db_mysql" {
  count                  = "${var.db_mysql_count}"
  ami                    = "${var.ami}"
  instance_type          = "t2.small"
  key_name               = "${var.key_name}"
  vpc_security_group_ids = ["${aws_security_group.db.id}"]
  subnet_id              = "${element(var.subnets_private, count.index)}"
  root_block_device = {
    volume_size     = "15"
    volume_type     = "gp2"
  }
  tags = {
    Name    = "db${count.index + 1}.voiceiq"
  }
}


