/* Route Table */
resource "aws_route_table" "private" {
    vpc_id = "${aws_vpc.voiceiq_vpc.id}"
    tags {
        Name = "VoiceIQ-Private"
    }
}

resource "aws_main_route_table_association" "private" {
    vpc_id = "${aws_vpc.voiceiq_vpc.id}"
    route_table_id = "${aws_route_table.private.id}"
}

/* Route Table Subnet Association */
resource "aws_route_table_association" "private-voiceiq-us-east-1a" {
    subnet_id = "${aws_subnet.private-voiceiq-us-east-1a.id}"
    route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "private-voiceiq-us-east-1b" {
    subnet_id = "${aws_subnet.private-voiceiq-us-east-1b.id}"
    route_table_id = "${aws_route_table.private.id}"
}

/* Routes on Route Table */
resource "aws_route" "private-stg-default" {
    route_table_id = "${aws_route_table.private.id}"
    destination_cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.voiceiq_nat.id}"
    depends_on = ["aws_route_table.private"]
}

