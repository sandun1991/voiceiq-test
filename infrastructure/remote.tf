terraform {
  backend "s3" {
    bucket = "voiceiq"
    key    = "infrastructure/use1/voiceiq_test/terraform.tfstate"
    region = "us-east-1"
    access_key = "<<>>"
    secret_key = "<<>>"
  }
}
