/* Route Table */
resource "aws_route_table" "public" {
    vpc_id = "${aws_vpc.voiceiq_vpc.id}"
    tags {
        Name = "VoiceIQ-Public"
    }
}

/* Route Table Subnet Association */
resource "aws_route_table_association" "public-voiceiq-us-east-1a" {
    subnet_id = "${aws_subnet.public-voiceiq-us-east-1a.id}"
    route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "public-voiceiq-us-east-1b" {
    subnet_id = "${aws_subnet.public-voiceiq-us-east-1b.id}"
    route_table_id = "${aws_route_table.public.id}"
}


/* Routes on Route Table */
resource "aws_route" "public-default" {
    route_table_id = "${aws_route_table.public.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.voiceiq_igw.id}"
    depends_on = ["aws_route_table.public"]
}

