resource "aws_subnet" "public-voiceiq-us-east-1a" {
    vpc_id = "${aws_vpc.voiceiq_vpc.id}"
    cidr_block = "10.1.0.0/24"
    availability_zone = "us-east-1a"
    tags {
        Name = "VoiceIQ-Public-us-east-1a"
    }
}

resource "aws_subnet" "public-voiceiq-us-east-1b" {
    vpc_id = "${aws_vpc.voiceiq_vpc.id}"
    cidr_block = "10.1.1.0/24"
    availability_zone = "us-east-1b"
    tags {
        Name = "VoiceIQ-Public-us-east-1b"
    }
}
