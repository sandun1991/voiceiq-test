/* VPN Security Group */
resource "aws_security_group" "vpn" {
  vpc_id = "${aws_vpc.voiceiq_vpc.id}"
  name = "VoiceIQ-VPN"
  description = "VPN Server"
  tags {
      name = "VoiceIQ-VPN"
  }
}

resource "aws_security_group_rule" "vpn_22_in" {
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "TCP"
    cidr_blocks = ["61.245.169.54/32"]
    security_group_id = "${aws_security_group.vpn.id}"
}

resource "aws_security_group_rule" "vpn_80_in" {
    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "TCP"
    cidr_blocks = ["61.245.169.54/32"]
    security_group_id = "${aws_security_group.vpn.id}"
}

resource "aws_security_group_rule" "vpn_all_out" {
    type = "ingress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = "${aws_security_group.vpn.id}"
}

/* Web Servers Security Group */
resource "aws_security_group" "web" {
  vpc_id = "${aws_vpc.voiceiq_vpc.id}"
  name = "VoiceIQ-Web"
  description = "Web Servers"
  tags {
      name = "VoiceIQ-Web"
  }
}

resource "aws_security_group_rule" "web_80_in" {
    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = "${aws_security_group.web.id}"
}

resource "aws_security_group_rule" "web_22_in" {
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "TCP"
    cidr_blocks = ["${aws_instance.vpn.private_ip}/32"]
    security_group_id = "${aws_security_group.web.id}"
}

resource "aws_security_group_rule" "web_all_out" {
    type = "ingress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = "${aws_security_group.vpn.id}"
}


/* DB Servers Security Group */
resource "aws_security_group" "db" {
  vpc_id = "${aws_vpc.voiceiq_vpc.id}"
  name = "VoiceIQ-DB"
  description = "DB Servers"
  tags {
      name = "VoiceIQ-DB"
  }
}

resource "aws_security_group_rule" "db_3306_in" {
    type = "ingress"
    from_port = 3306
    to_port = 3306
    protocol = "TCP"
    cidr_blocks = ["10.1.11.0/24", "10.1.12.0/24"]
    security_group_id = "${aws_security_group.db.id}"
}

resource "aws_security_group_rule" "db_22_in" {
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "TCP"
    cidr_blocks = ["${aws_instance.vpn.private_ip}/32"]
    security_group_id = "${aws_security_group.db.id}"
}

resource "aws_security_group_rule" "db_all_out" {
    type = "ingress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = "${aws_security_group.db.id}"
}
