variable "web_app_count" {
  default = 2
}

/* Web Servers */
resource "aws_instance" "web_app" {
  count                  = "${var.web_app_count}"
  ami                    = "${var.ami}"
  instance_type          = "t2.micro"
  key_name               = "${var.key_name}"
  vpc_security_group_ids = ["${aws_security_group.web.id}"]
  subnet_id              = "${element(var.subnets_private, count.index)}"
  root_block_device = {
    volume_size     = "15"
    volume_type     = "gp2"
  }
  tags = {
    Name    = "web${count.index + 1}.voiceiq"
  }
}


/* Add instances into target group */
resource "aws_lb_target_group_attachment" "web_app_0" {
  target_group_arn = "${aws_lb_target_group.alb_default_tg.arn}"
  target_id        = "${aws_instance.web_app.0.id}"
  port             = 80
}
resource "aws_lb_target_group_attachment" "web_app_1" {
  target_group_arn = "${aws_lb_target_group.alb_default_tg.arn}"
  target_id        = "${aws_instance.web_app.1.id}"
  port             = 80
}

/* Add routing */
resource "aws_lb_listener_rule" "web_app_routing" {
  listener_arn = "${var.default_listener}"
  priority     = 10

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.alb_default_tg.arn}"
  }
  
  condition {
    field  = "host-header"
    values = ["www.voiceiq.com"]
  }

}
